/**
 * 水煤电
 * 水费：1134440256
 * 电费：0943000047848753
 * 燃气费：2605250000
 * 
 */

var crypto = require('crypto');
var md5 = require('md5');
var sd = require('silly-datetime');
var rp = require('request-promise')

var url = 'http://test.saiheyi.com/Stock/Post'
var account = '18026938456'
var password = '123456'
var partnerid = '103291'
var signKey = 'C7FA8FC2D9256DD6865ADFFA81249FEC'


function signFun(param) {
    var lists = [];

    for (item in param) {
        lists.push(item)
    }
    lists.sort();

    var signStr
    lists.forEach(function (key) {
        if (!!param[key]) {
            if (!signStr) {
                signStr = key + "=" + param[key];
            } else {
                signStr = signStr + "&" + key + "=" + param[key]
            }
        }

    })
    return  md5(signStr+signKey);
}

exports.query = function(req,res){
    var actionname = 'wecbillquery'
    var timestamp = sd.format(new Date(), 'YYYY/MM/DD HH:mm:ss');

    var stockordernumber = req.query.stockordernumber;

    var signParam = {
        stockordernumber : stockordernumber,
        actionname : actionname,
        timestamp : timestamp,
        partnerid : partnerid
    }

    var signStr = signFun(signParam)

    var option = {
        url : url,
        method: 'POST',
        form:{
            stockordernumber : stockordernumber,
            actionname : actionname,
            timestamp : timestamp,
            partnerid : partnerid,
            sign : signStr
        },
        json : true,
        headers : {'Content-Type': 'application/x-www-form-urlencoded'}
    }

    rp(option).then(function(result){
        res.send(result)
    })

}

exports.post = function(req,res){
    var wecaccount = req.query.wecaccount
    var ymonth = req.query.ymonth
    var stockordernumber = req.query.stockordernumber



    var actionname = 'wecbillpost'

    var productid = 'WEC08011513321649'
    var timestamp = sd.format(new Date(), 'YYYY-MM-DD HH:mm:ss');

    var signParam = {
        partnerid : partnerid,
        actionname : actionname,
        productid : productid,
        timestamp : timestamp,
        stockordernumber : stockordernumber,
        wecaccount : wecaccount,
        ymonth : ymonth
    }

    var signStr = signFun(signParam)

    var option = {
        url : url,
        method: 'POST',
        headers : {
            'Content-Type' : 'application/x-www-form-urlencoded'
        },
        form:{
            partnerid : partnerid,
            actionname : actionname,
            productid : productid,
            timestamp : timestamp,
            stockordernumber : stockordernumber,
            wecaccount : wecaccount,
            ymonth : ymonth,
            sign : signStr
        },
        json : true
    }

    rp(option).then(function(result){
        res.send(result)
    })
}