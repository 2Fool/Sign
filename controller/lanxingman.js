/**
 * 蓝星漫星座sign加密
 */

var crypto = require('crypto');
var md5 = require('md5');
var sd = require('silly-datetime');
var rp = require('request-promise')

var appId = 'mx3424bf3949fadb67b'
var appSecrect = '056cfa466a754736a42e5c6c974829e9'
var channelSign = 'qwqiwuwsgl2'
var productId = 'e8afe20defc244bab9c4b576f3295b4c'

function signFun(param) {
    var lists = [];

    for (item in param) {
        lists.push(item)
    }
    lists.sort();

    var signStr
    lists.forEach(function (key) {
        if (!!param[key]) {
            if (!signStr) {
                signStr = key + "=" + param[key];
            } else {
                signStr = signStr + "&" + key + "=" + param[key]
            }
        }

    })
    md5SignStr =  md5(signStr);

    str =  md5SignStr + appSecrect;

    var sign = md5(str) ;

    return sign;
}

function infoSign(param) {
    var lists = [];
    for (item in param) {
        lists.push(item)
    }
    lists.sort();

    var infoSignStr
    lists.forEach(function (key) {

        if (!!param[key]) {
            if (!infoSignStr) {
                infoSignStr = key + "=" + param[key];
            } else {
                infoSignStr = infoSignStr + "&" + key + "=" + param[key]
            }
        }

    })
    return md5(infoSignStr);
}

exports.constellation = function (req, res) {

    var infoSignStrs;
    if (req.query.isDouble == 0) {
        var infoParam = {
            username1: req.query.username1,
            birthTime1: req.query.birthTime1,
            birthLocation1: req.query.birthLocation1,
            liveTime1: req.query.liveTime1,
            liveLocation1: req.query.liveLocation1,
            sex1: req.query.sex1,
            bizUserBirthLunar: req.query.bizUserBirthLunar
        }
        infoSignStrs = infoSign(infoParam)
    }


    var param = {
        appId: appId,
        isDouble: req.query.isDouble,
        channelSign: channelSign,
        productId: productId,
        loveStatus: req.query.loveStatus,
        ip : req.query.ip,
        infoSign : infoSignStrs,

        username1: req.query.username1,
        birthTime1: req.query.birthTime1,
        birthLocation1: req.query.birthLocation1,
        liveTime1: req.query.liveTime1,
        liveLocation1: req.query.liveLocation1,
        sex1: req.query.sex1,
        bizUserBirthLunar: req.query.bizUserBirthLunar,

        // username2: req.query.username2,
        // birthTime2: req.query.birthTime2,
        // birthLocation2: req.query.birthLocation2,
        // liveTime2: req.query.liveTime2,
        // liveLocation2: req.query.liveLocation2,
        // sex2: req.query.sex2,
        // bizUserBirthLunar2: req.query.bizUserBirthLunar2

    }

    var signStr = signFun(param);

    var url = 'https://jupi.debug.muxingnet.com/api/calculate/sdkApi/genReport?authToken=60'

    var options = {
        url: url,
        method: 'POST',
        body: {
            appId: appId,
            sign: signStr,
            isDouble: req.query.isDouble,
            channelSign: channelSign,
            productId: productId,
            infoSign: infoSignStrs,
            loveStatus: req.query.loveStatus,
            ip : req.query.ip,

            username1: req.query.username1,
            birthTime1: req.query.birthTime1,
            birthLocation1: req.query.birthLocation1,
            liveTime1: req.query.liveTime1,
            liveLocation1: req.query.liveLocation1,
            sex1: req.query.sex1,
            bizUserBirthLunar: req.query.bizUserBirthLunar,

            // username2: req.query.username2,
            // birthTime2: req.query.birthTime2,
            // birthLocation2: req.query.birthLocation2,
            // liveTime2: req.query.liveTime2,
            // liveLocation2: req.query.liveLocation2,
            // sex2: req.query.sex2,
            // bizUserBirthLunar2: req.query.bizUserBirthLunar2
        },
        json: true
    }

    rp(options).then(function (result) {
        res.send(result);
    })

}