// TypeScript
import AlipaySdk from 'alipay-sdk';
var fs = require('fs')

const alipaySdk = new AlipaySdk({
    appId: '2016123456789012',
    privateKey: fs.readFileSync('./private-key.pem', 'ascii'),
    alipayPublicKey: fs.readFileSync('./public-key.pem', 'ascii'),
});

// JS
const AlipaySdk = require('alipay-sdk').default;

const alipaySdk = new AlipaySdk({
    appId: '2019042564269580',
    privateKey: fs.readFileSync('./private-key.pem', 'ascii'),
    alipayPublicKey: fs.readFileSync('../key/alipayPublicKey.pem', 'ascii'),
});